package cache

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"github.com/ptflp/godecoder"
	"github.com/redis/go-redis/v9"
	"time"
)

const defaultReconnectionTimeout = 5

type redisError struct {
	err     error
	command command
}

// GetCommand retrieves cache command associated with error as string
func (r *redisError) GetCommand() string {
	return string(r.command)
}

func (r *redisError) Error() string {
	return fmt.Sprintf("redis: error %s occured on attempt to perform command %s", r.err, r.command)
}

// RedisCache wraps *redis.Client to meet swappable and mockable Cache interface
type RedisCache struct {
	rdb          redis.UniversalClient
	cacheErrors  chan Error
	broadcasting bool
	decoder      godecoder.Decoder
}

func (c *RedisCache) Delete(ctx context.Context, pattern bool, keys ...string) error {
	if pattern {
		for _, key := range keys {
			err := c.deletePattern(ctx, key)
			if err != nil {
				return err
			}
		}
	} else {
		err := c.rdb.Del(ctx, keys...).Err()
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *RedisCache) deletePattern(ctx context.Context, pattern string) error {
	var cursor uint64
	var err error
	for {
		// scan for keys
		var keys []string
		keys, cursor, err = c.rdb.Scan(ctx, cursor, pattern, 0).Result()
		if err != nil {
			return err
		}
		// if we have found some keys, delete them
		if len(keys) > 0 {
			err = c.rdb.Del(ctx, keys...).Err()
			if err != nil {
				return err
			}
		}
		// if cursor is 0, it means that we have finished scanning
		if cursor == 0 {
			break
		}
	}

	return nil
}

// Get retrieves value from Redis and serializes to pointer value
func (c *RedisCache) Get(ctx context.Context, key string, ptrValue interface{}) error {
	b, err := c.rdb.Get(ctx, key).Bytes()
	if err != nil {
		if errors.Is(err, redis.Nil) {
			return ErrCacheMiss
		}
		if c.broadcasting {
			go c.broadcastError(&redisError{err, get})
		}
		return err
	}
	buffer := bytes.NewBuffer(b)

	return c.decoder.Decode(buffer, ptrValue)
}

// Set takes key and value as input and setting Redis cache with this value
// shares errors to channel in case broadcasting mode is enabled
// it prevents manual error handling which seems noisy and unnecessary in rdb code
func (c *RedisCache) Set(ctx context.Context, key string, value interface{}, expires time.Duration) error {
	var data []byte
	var ok bool
	if data, ok = value.([]byte); !ok {
		var b bytes.Buffer
		err := c.decoder.Encode(&b, value)
		if err != nil && c.broadcasting {
			c.cacheErrors <- &redisError{err, set}
			return err
		}
		data = b.Bytes()
	}
	if err := c.rdb.Set(ctx, key, data, expires).Err(); err != nil {
		if c.broadcasting {
			c.cacheErrors <- &redisError{err, set}
		}
		return err
	}

	return nil
}

// Expire updates TTL for specified key
func (c *RedisCache) Expire(ctx context.Context, key string, expiration time.Duration) error {
	if err := c.rdb.Expire(ctx, key, expiration).Err(); err != nil {
		if c.broadcasting {
			go c.broadcastError(&redisError{err, expire})
		}
		return err
	}

	return nil
}

// KeyExists checks whether specified key exists; returns true and nil if key is actually exists
func (c *RedisCache) KeyExists(ctx context.Context, key string) (bool, error) {

	err := c.rdb.Get(ctx, key).Err()
	if errors.Is(err, redis.Nil) {
		return false, ErrCacheMiss
	}
	if err != nil && c.broadcasting {
		go c.broadcastError(&redisError{err, keyExists})
	}
	return true, err
}

func (c *RedisCache) broadcastError(err Error) {
	c.cacheErrors <- err
}
