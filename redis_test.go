package cache

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-redis/redismock/v9"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"io"
	"testing"
	"time"
)

type MockRedisClient struct {
	ScanFunc   func(ctx context.Context, cursor uint64, match string, count int64) *redis.ScanCmd
	DelFunc    func(ctx context.Context, keys ...string) *redis.IntCmd
	PingFunc   func(ctx context.Context) *redis.StatusCmd
	ExistsFunc func(ctx context.Context, keys ...string) *redis.IntCmd
	SetFunc    func(ctx context.Context, key string, value interface{}, expiration time.Duration) *redis.StatusCmd
	GetFunc    func(ctx context.Context, key string) *redis.StringCmd
	ExpireFunc func(ctx context.Context, key string, expiration time.Duration) *redis.BoolCmd
	redis.UniversalClient
}

func (m *MockRedisClient) Expire(ctx context.Context, key string, expiration time.Duration) *redis.BoolCmd {
	return m.ExpireFunc(ctx, key, expiration)
}

func (m *MockRedisClient) Get(ctx context.Context, key string) *redis.StringCmd {
	return m.GetFunc(ctx, key)
}

func (m *MockRedisClient) Ping(ctx context.Context) *redis.StatusCmd {
	return m.PingFunc(ctx)
}

func (m *MockRedisClient) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) *redis.StatusCmd {
	return m.SetFunc(ctx, key, value, expiration)
}

func (m *MockRedisClient) Exists(ctx context.Context, keys ...string) *redis.IntCmd {
	return m.ExistsFunc(ctx, keys...)
}

func (m *MockRedisClient) Scan(ctx context.Context, cursor uint64, match string, count int64) *redis.ScanCmd {
	return m.ScanFunc(ctx, cursor, match, count)
}

func (m *MockRedisClient) Del(ctx context.Context, keys ...string) *redis.IntCmd {
	return m.DelFunc(ctx, keys...)
}

func TestTableRedisCache_Delete(t *testing.T) {
	testCases := []struct {
		name           string
		pattern        bool
		keys           []string
		expectedError  error
		expectedDelete []string
	}{
		{
			name:           "Delete with pattern",
			pattern:        true,
			keys:           []string{"key1", "key2"},
			expectedError:  nil,
			expectedDelete: []string{"key1", "key2"},
		},
		{
			name:           "Delete without pattern",
			pattern:        false,
			keys:           []string{"key3", "key4"},
			expectedError:  nil,
			expectedDelete: []string{"key3", "key4"},
		},
		{
			name:           "No such key",
			pattern:        false,
			keys:           []string{"key5"},
			expectedError:  errors.New("no such key"),
			expectedDelete: []string{},
		},
		{
			name:           "No such pattern",
			pattern:        true,
			keys:           []string{"key6"},
			expectedError:  errors.New("no such key"),
			expectedDelete: []string{},
		},
	}

	mockClient := &MockRedisClient{}
	mockClient.ScanFunc = func(ctx context.Context, cursor uint64, match string, count int64) *redis.ScanCmd {
		keys := []string{"key1", "key2", "key3"}
		if match == "key6" {
			scanCmd := redis.NewScanCmd(ctx, nil)
			scanCmd.SetErr(errors.New("no such key"))
			return scanCmd
		}
		return redis.NewScanCmd(ctx, nil, cursor, match, count, keys)
	}

	mockClient.DelFunc = func(ctx context.Context, keys ...string) *redis.IntCmd {
		for _, key := range keys {
			if key == "key5" {
				intCmd := redis.NewIntCmd(ctx, keys)
				intCmd.SetErr(errors.New("no such key"))
				return intCmd
			}
		}
		return redis.NewIntCmd(ctx, 0, nil)
	}

	cache := &RedisCache{
		rdb:     mockClient,
		decoder: godecoder.NewDecoder(jsoniter.Config{}),
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.Background()

			if err := cache.Delete(ctx, tc.pattern, tc.keys...); err != nil {
				assert.Equal(t, tc.expectedError, err)
			}
		})
	}
}

func TestTableRedisCache_Expire(t *testing.T) {
	decoder := godecoder.NewDecoder(jsoniter.Config{})
	ctx := context.Background()
	mockClient := &MockRedisClient{}
	mockClient.ExpireFunc = func(ctx context.Context, key string, expiration time.Duration) *redis.BoolCmd {
		if key == "test1" {
			boolCmd := redis.NewBoolCmd(ctx, key, expiration)
			boolCmd.SetErr(errors.New("no such key"))
			return boolCmd
		}
		return redis.NewBoolCmd(ctx, key, expiration)
	}

	type fields struct {
		rdb          redis.UniversalClient
		cacheErrors  chan Error
		broadcasting bool
		decoder      godecoder.Decoder
	}
	type args struct {
		ctx        context.Context
		key        string
		expiration time.Duration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "valid output",
			fields: fields{
				rdb:     mockClient,
				decoder: decoder,
			},
			args: args{
				ctx,
				"test",
				time.Minute,
			},
			wantErr: false,
		},
		{
			name: "Invalid output",
			fields: fields{
				rdb:     mockClient,
				decoder: decoder,
			},
			args: args{
				ctx,
				"test1",
				time.Minute,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &RedisCache{
				rdb:          tt.fields.rdb,
				cacheErrors:  tt.fields.cacheErrors,
				broadcasting: tt.fields.broadcasting,
				decoder:      tt.fields.decoder,
			}
			if err := c.Expire(tt.args.ctx, tt.args.key, tt.args.expiration); (err != nil) != tt.wantErr {
				t.Errorf("Expire() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTableRedisCache_Get(t *testing.T) {
	ctx := context.Background()
	decoder := godecoder.NewDecoder(jsoniter.Config{})
	mockClient := &MockRedisClient{}
	mockClient.GetFunc = func(ctx context.Context, key string) *redis.StringCmd {
		if key == "test1" {
			stringCmd := redis.NewStringCmd(ctx, key)
			stringCmd.SetErr(redis.Nil)
			return stringCmd
		} else if key == "test2" {
			stringCmd := redis.NewStringCmd(ctx, key)
			stringCmd.SetErr(errors.New("no such key"))
			return stringCmd
		}
		stringCmd := redis.NewStringCmd(ctx, key)
		stringCmd.SetVal("\"value\"")
		return stringCmd
	}
	var res string

	type fields struct {
		rdb          redis.UniversalClient
		cacheErrors  chan Error
		broadcasting bool
		decoder      godecoder.Decoder
	}
	type args struct {
		ctx      context.Context
		key      string
		ptrValue interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "key is found",
			fields: fields{
				rdb:          mockClient,
				decoder:      decoder,
				broadcasting: true,
				cacheErrors:  make(chan Error, 1),
			},
			args: args{
				ctx:      ctx,
				key:      "test",
				ptrValue: &res,
			},
			wantErr: false,
		},
		{
			name: "key is not found",
			fields: fields{
				rdb:          mockClient,
				decoder:      decoder,
				broadcasting: true,
				cacheErrors:  make(chan Error, 1),
			},
			args: args{
				ctx:      ctx,
				key:      "test1",
				ptrValue: &res,
			},
			wantErr: true,
		},
		{
			name: "key is not found",
			fields: fields{
				rdb:          mockClient,
				decoder:      decoder,
				broadcasting: true,
				cacheErrors:  make(chan Error, 1),
			},
			args: args{
				ctx:      ctx,
				key:      "test2",
				ptrValue: &res,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &RedisCache{
				rdb:          tt.fields.rdb,
				cacheErrors:  tt.fields.cacheErrors,
				broadcasting: tt.fields.broadcasting,
				decoder:      tt.fields.decoder,
			}
			if err := c.Get(tt.args.ctx, tt.args.key, tt.args.ptrValue); (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestRedisCache_KeyExists(t *testing.T) {
	ctx := context.Background()
	mockClient := &MockRedisClient{}
	mockClient.GetFunc = func(ctx context.Context, key string) *redis.StringCmd {
		if key == "test1" {
			stringCmd := redis.NewStringCmd(ctx, key)
			stringCmd.SetErr(redis.Nil)
			return stringCmd
		}
		return redis.NewStringCmd(ctx, key)
	}
	decoder := godecoder.NewDecoder(jsoniter.Config{})

	type fields struct {
		rdb          redis.UniversalClient
		cacheErrors  chan Error
		broadcasting bool
		decoder      godecoder.Decoder
	}
	type args struct {
		ctx context.Context
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "key found",
			fields: fields{
				rdb:     mockClient,
				decoder: decoder,
			},
			args: args{
				ctx: ctx,
				key: "test",
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "no such key found",
			fields: fields{
				rdb:     mockClient,
				decoder: decoder,
			},
			args: args{
				ctx: ctx,
				key: "test1",
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &RedisCache{
				rdb:          tt.fields.rdb,
				cacheErrors:  tt.fields.cacheErrors,
				broadcasting: tt.fields.broadcasting,
				decoder:      tt.fields.decoder,
			}
			got, err := c.KeyExists(tt.args.ctx, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("KeyExists() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("KeyExists() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTableRedisCache_Set(t *testing.T) {
	mockClient := &MockRedisClient{}
	decoder := godecoder.NewDecoder(jsoniter.Config{})
	ctx := context.Background()

	mockClient.SetFunc = func(ctx context.Context, key string, value interface{}, expiration time.Duration) *redis.StatusCmd {
		if key == "test1" {
			statusCmd := redis.NewStatusCmd(ctx)
			statusCmd.SetErr(errors.New("such is already key exists"))
			return statusCmd
		}
		return redis.NewStatusCmd(ctx, key, value, expiration)
	}

	type fields struct {
		rdb          redis.UniversalClient
		cacheErrors  chan Error
		broadcasting bool
		decoder      godecoder.Decoder
	}
	type args struct {
		ctx     context.Context
		key     string
		value   interface{}
		expires time.Duration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Valid output",
			fields: fields{
				rdb:     mockClient,
				decoder: decoder,
			},
			args: args{
				ctx:     ctx,
				key:     "test",
				value:   "value",
				expires: time.Hour,
			},
			wantErr: false,
		},
		{
			name: "Invalid output",
			fields: fields{
				rdb:     mockClient,
				decoder: decoder,
			},
			args: args{
				ctx:     ctx,
				key:     "test1",
				value:   "value",
				expires: time.Hour,
			},
			wantErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			c := &RedisCache{
				rdb:          tc.fields.rdb,
				cacheErrors:  tc.fields.cacheErrors,
				broadcasting: tc.fields.broadcasting,
				decoder:      tc.fields.decoder,
			}
			if err := c.Set(tc.args.ctx, tc.args.key, tc.args.value, tc.args.expires); (err != nil) != tc.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tc.wantErr)
			}
		})
	}
}

func TestTableRedisCache_broadcastError(t *testing.T) {
	client := &MockRedisClient{}

	type fields struct {
		rdb          redis.UniversalClient
		cacheErrors  chan Error
		broadcasting bool
		decoder      godecoder.Decoder
	}
	type args struct {
		err Error
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "valid output",
			fields: fields{
				rdb:          client,
				decoder:      godecoder.NewDecoder(jsoniter.Config{}),
				cacheErrors:  make(chan Error, 1),
				broadcasting: true,
			},
			args: args{
				err: &redisError{
					err:     errors.New("test error"),
					command: "get",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &RedisCache{
				rdb:          tt.fields.rdb,
				cacheErrors:  tt.fields.cacheErrors,
				broadcasting: tt.fields.broadcasting,
				decoder:      tt.fields.decoder,
			}
			c.broadcastError(tt.args.err)
		})
	}
}

func TestRedisCache_deletePattern(t *testing.T) {
	ctx := context.Background()

	mockClient := &MockRedisClient{}

	mockClient.ScanFunc = func(ctx context.Context, cursor uint64, match string, count int64) *redis.ScanCmd {
		keys := []string{"key1", "key2", "key3"}
		return redis.NewScanCmd(ctx, nil, cursor, match, count, keys)
	}

	mockClient.DelFunc = func(ctx context.Context, keys ...string) *redis.IntCmd {
		expectedKeys := []string{"key1", "key2", "key3"}
		if len(keys) != len(expectedKeys) {
			t.Errorf("Expected %d keys to be deleted, but got %d", len(expectedKeys), len(keys))
		}
		for i, key := range keys {
			if key != expectedKeys[i] {
				t.Errorf("Expected key %s to be deleted, but got %s", expectedKeys[i], key)
			}
		}
		return nil
	}

	cache := &RedisCache{
		rdb: mockClient,
	}

	err := cache.deletePattern(ctx, "test")
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
}

func Test_redisError_Error(t *testing.T) {
	type fields struct {
		err     error
		command command
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "valid output",
			fields: fields{
				err:     errors.New("test error"),
				command: "get",
			},
			want: "redis: error test error occured on attempt to perform command get",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &redisError{
				err:     tt.fields.err,
				command: tt.fields.command,
			}
			if got := r.Error(); got != tt.want {
				t.Errorf("Error() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_redisError_GetCommand(t *testing.T) {
	type fields struct {
		err     error
		command command
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "valid output",
			fields: fields{
				err:     errors.New("test error"),
				command: "get",
			},
			want: "get",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &redisError{
				err:     tt.fields.err,
				command: tt.fields.command,
			}
			if got := r.GetCommand(); got != tt.want {
				t.Errorf("GetCommand() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedisCache_Delete(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "1"
	mock.ExpectDel(key).SetVal(0)
	err := mockRedisCache.Delete(context.Background(), false, key)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}

func TestRedisCache_Delete2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "2"
	mock.ExpectScan(0, key, 0).SetVal([]string{}, 0)
	err := mockRedisCache.Delete(context.Background(), true, key)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Delete2_2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "2"
	mock.ExpectScan(0, key, 0).SetErr(errors.New("1"))
	err := mockRedisCache.Delete(context.Background(), true, key)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Delete2_3(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "2"
	mock.ExpectScan(0, key, 0).SetVal([]string{"1", "2"}, 0)
	mock.ExpectDel("1", "2").SetErr(errors.New("1"))
	err := mockRedisCache.Delete(context.Background(), true, key)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Delete3(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "2"
	mock.ExpectScan(0, key, 0).SetVal([]string{"2", "3"}, 0)
	mock.ExpectDel("2", "3").SetVal(0)
	err := mockRedisCache.Delete(context.Background(), true, key)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Delete4(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      nil,
	}
	key := "1"
	mock.ExpectDel(key).SetErr(errors.New("ExpectDel"))
	err := mockRedisCache.Delete(context.Background(), false, key)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Get(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetVal("0")
	err := mockRedisCache.Get(context.Background(), key, nil)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}

}

func TestRedisCache_Get2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetErr(errors.New("1"))
	err := mockRedisCache.Get(context.Background(), key, nil)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}

}
func TestRedisCache_Get2_2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetErr(redis.Nil)
	err := mockRedisCache.Get(context.Background(), key, nil)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}

}
func TestRedisCache_Get2_3(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetErr(errors.New("1"))
	err := mockRedisCache.Get(context.Background(), key, nil)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}

}
func TestRedisCache_Set(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectSet(key, key, 1*time.Second).SetVal("0")
	err := mockRedisCache.Set(context.Background(), key, nil, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}

func TestRedisCache_Set2(t *testing.T) {
	var data []byte
	db, mock := redismock.NewClientMock()
	ch := make(chan Error)
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  ch,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"

	go func() {
		<-ch
	}()
	mock.ExpectSet(key, key, 1*time.Second).SetErr(errors.New("1"))
	err := mockRedisCache.Set(context.Background(), key, data, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Set3(t *testing.T) {
	data := []byte{'1'} // Передаем строку, а не массив байтов
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"

	mock.ExpectSet(key, data, 1*time.Second).SetVal("1") // Здесь мы также передаем строку, а не ключ
	err := mockRedisCache.Set(context.Background(), key, []byte(data), 1*time.Second)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}

type MyJSONDecoder2 struct{}

func (d *MyJSONDecoder2) Decode(r io.Reader, val interface{}) error {
	// Имитация корректной остановки сервера
	return nil
}

func (d *MyJSONDecoder2) Encode(w io.Writer, value interface{}) error {
	// Имитация корректной остановки сервера
	return errors.New("1")
}
func TestRedisCache_Set4(t *testing.T) {
	var data bool // Передаем строку, а не массив байтов
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  make(chan Error),
		broadcasting: true,
		decoder:      &MyJSONDecoder2{},
	}
	key := "1"
	go func() {
		<-mockRedisCache.cacheErrors
	}()
	mock.ExpectSet(key, data, 1*time.Second).SetVal("1") // Здесь мы также передаем строку, а не ключ
	err := mockRedisCache.Set(context.Background(), key, data, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Expire(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectExpire(key, 1*time.Second).SetErr(errors.New("1"))
	err := mockRedisCache.Expire(context.Background(), key, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Expire2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectExpire(key, 1*time.Second).SetErr(errors.New("1"))
	err := mockRedisCache.Expire(context.Background(), key, 1*time.Second)
	if err == nil {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisCache_Expire3(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectExpire(key, 1*time.Second).SetVal(true)
	err := mockRedisCache.Expire(context.Background(), key, 1*time.Second)
	if err != nil {
		t.Errorf("Ошибка: %v", err)
	}
}

type MyJSONDecoder struct{}

func (d *MyJSONDecoder) Decode(r io.Reader, val interface{}) error {
	// Имитация корректной остановки сервера
	return nil
}

func (d *MyJSONDecoder) Encode(w io.Writer, value interface{}) error {
	// Имитация корректной остановки сервера
	return nil
}

func TestRedisCache_broadcastError(t *testing.T) {
	// Создаем мок клиента Redis
	db, _ := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  make(chan Error),
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}

	// Определяем ошибку для тестирования
	errRedis := &redisError{
		err:     errors.New("1"),
		command: "err",
	}

	// Вызываем метод broadcastError с тестовой ошибкой
	go func() {
		mockRedisCache.broadcastError(errRedis)
		close(mockRedisCache.cacheErrors) // Закрываем канал после отправки ошибки
	}()

	// Проверяем, что ошибка получена из канала
	select {
	case errFromChannel := <-mockRedisCache.cacheErrors:
		if errFromChannel != errRedis {
			t.Errorf("Received error %v, expected %v", errFromChannel, errRedis)
		}
	case <-time.After(1 * time.Second):
		t.Error("Did not receive error within 1 second")
	}
}
func TestRedisError_Error(t *testing.T) {
	test := &redisError{
		err:     nil,
		command: "123",
	}
	r := test.Error()
	if r == fmt.Sprintf("redis: ошибка %s произошла при попытке выполнить команду %s", test.err, test.command) {
		t.Errorf("func (r *redisError) Error() string")
	}
}
func TestRedisError_GetCommand(t *testing.T) {
	test := &redisError{
		err:     nil,
		command: "123",
	}
	r := test.GetCommand()
	if r != string(test.command) {
		t.Errorf("func TestRedisError_GetCommand(t *testing.T)")
	}
}
func TestRedisError_KeyExists(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: false,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	mock.ExpectGet(key).SetErr(redis.Nil)
	b, err := mockRedisCache.KeyExists(context.Background(), key)
	if b != false || err != ErrCacheMiss {
		t.Errorf("Ошибка: %v", err)
	}
}
func TestRedisError_KeyExists2(t *testing.T) {
	db, mock := redismock.NewClientMock()
	mockRedisCache := &RedisCache{
		rdb:          db,
		cacheErrors:  nil,
		broadcasting: true,
		decoder:      &MyJSONDecoder{},
	}
	key := "1"
	nerr := errors.New("0")
	mock.ExpectGet(key).SetErr(nerr)
	b, err := mockRedisCache.KeyExists(context.Background(), key)
	if b != true || err != nerr {
		t.Errorf("Ошибка: %v", err)
	}
}
